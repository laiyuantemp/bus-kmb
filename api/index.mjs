import {kv} from '@vercel/kv' //for update_stop_list() cache

import express from 'express'
const app = express()

let unique_by_key = (arr, key) => [...new Map(arr.map(item =>[item[key], item])).values()]

const BASE_URL = 'https://data.etabus.gov.hk/v1'
class Bus_kmb{
    constructor(is_zh=true){
        this.is_zh = is_zh
    }

    //this.stop_list; this.update_timestamp
    async update_stop_list(mode=null){  //default:null, update, reload
        if(this.stop_list && !mode)
            return
        switch(mode){
            case 'update':{
                var res = await (await fetch(`${BASE_URL}/transport/kmb/stop`)).json()
                var results = res.data.map(({stop, name_en, name_tc, name_sc})=>({
                    stop,
                    name_en,
                    name_tc,
                    name_sc
                }))

                this.stop_list = results
                this.update_timestamp = res.generated_timestamp

                await kv.set('bus-kmb', {
                    'stop_list': this.stop_list,
                    'update_timestamp': this.update_timestamp
                })
                break
            }

            default:    //when empty this.stop_list
            case 'reload':{
                const value = await kv.get('bus-kmb')
                if(!value)
                    await this.update_stop_list('update')
                else{
                    this.stop_list = value['stop_list']
                    this.update_timestamp = value['update_timestamp']
                }
                break
            }
        }
    }

    async html_stop_list(){
        await this.update_stop_list('reload')

        var html = '<form action="/stop/choose/getall" method="get">'
        html += '<input type="text" name="stop" list="stopList" placeholder="站名" /><datalist id="stopList">'
        this.stop_list.forEach(e=>{
            html += `<option value="${e.stop}">${e.name_en}</option>`
            html += `<option value="${e.stop}">${e.name_tc}</option>`
            html += `<option value="${e.stop}">${e.name_sc}</option>`
        })
        html += '</datalist><br><br><input type="submit"></form>'
        return html
    }


    async get_route_list(route, direction, is_html){
        await this.update_stop_list('reload')

        const this_url = `${BASE_URL}/transport/kmb/route-stop/${route.toUpperCase()}/${direction}`
        const data1 = (await (await fetch(`${this_url}/1`)).json())['data']
        const data2 = (await (await fetch(`${this_url}/2`)).json())['data']
        var data = data1.concat(data2)
        data = unique_by_key(data, 'stop')
        for(var d of data){
            const matched_stop = this.stop_list.filter(x=> x.stop === d.stop)
            d.name_tc = matched_stop[0].name_tc
        }

        if(is_html){
            const reverse_dir = direction === 'inbound' ? 'outbound' : 'inbound'
            var html = 
`<a href="/route/choose?route=${route}&direction=${reverse_dir}">反方向</a><br><br>
<form action="/route/choose/get" method="get">
<input type="text" name="route" value="${route}" hidden>
[起點]<br>`
            data.forEach(e=>{
                html += `<input type="radio" name="stop" value="${e.stop}" id="${e.stop}">`
                html += `<label for="${e.stop}">${e.name_tc}</label><br>`
            })
            html += '[終點]<br><br><input type="submit"></form>'
            return html
        }
        return data
    }

    async transform_route_at_stop_result(data, is_html){
        //unique
        data = [...new Map(data.map(item =>[item['route']+item['eta'], item])).values()]

        var results = data.map(({route,dest_tc,eta,rmk_tc})=>{
            var arrive_dt_strg, eta_offset
            if(eta){
                var arrive_dt = new Date(eta)
                arrive_dt_strg = arrive_dt.toLocaleTimeString('en-GB', {hour: '2-digit', minute:'2-digit', second:'2-digit', timeZone: 'Asia/Hong_Kong'})
                eta_offset = Math.round((arrive_dt - Date.now())/1000/60) //in min
            }else
                arrive_dt_strg = eta_offset = Infinity

            var return_json = {
                route,
                dest_tc,
                'eta': arrive_dt_strg,
                eta_offset,
            }
            if(rmk_tc)
                return_json['rmk_tc'] = rmk_tc
            return return_json
        })

        results.sort((a, b) => a.eta_offset - b.eta_offset)

        const zh_arr = {
            'route': '路線',
            'dest_tc': '終點',
            'eta': '到站時間',
            'eta_offset': '相差(分鍾)',
            'rmk_tc': '備注'
        }
        if(this.is_zh)
            results.forEach((e,i,arr)=>{
                var zh_message = {}
                Object.entries(e).forEach(([k,v]) => {
                    if(zh_arr.hasOwnProperty(k))
                        zh_message[zh_arr[k]] = v
                    else
                        zh_message[k] = v

                })
                arr[i] = zh_message
            })

        if(is_html){
            var html = '<pre>'
            var json_strg = results.map(e=>(JSON.stringify(e,null,2)))
            html += json_strg.join(',\n')
            html += '</pre>'
            return html
        }
        return results
    }
    async get_all_route_at_stop(stop, is_html){
        var res = await (await fetch(`${BASE_URL}/transport/kmb/stop-eta/${stop}`)).json()
        var results = await this.transform_route_at_stop_result(res.data, is_html)
        return results
    }

    async get_route_at_stop(route, stop, is_html){
        const this_url = `${BASE_URL}/transport/kmb/eta/${stop}/${route.toUpperCase()}`
        var data1 = (await (await fetch(`${this_url}/1`)).json())['data']
        var data2 = (await (await fetch(`${this_url}/1`)).json())['data']
        var data = data1.concat(data2)

        var results = await this.transform_route_at_stop_result(data, is_html)
        if(is_html)
            results = `<a href="/stop/choose/getall?stop=${stop}">此站其他路線</a><br>` + results
        return results
    }

}

function inject_css(body){
    return `<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body{
    -webkit-transform: scale(1.3);
    transform: scale(1.3);
    transform-origin: 0 0;
    color: white;
    background-color: black;
}
a{color: yellow;}
</style></head>
<body>${body}<body>`
}

app.get('/', async (req, res) => {
    const index_html = '<a href="/stop">站名</a><br><br><a href="/route">路線</a><br><br><a href="/stop/reload">_</a>'
    res.send(inject_css(index_html))
})

app.get('/stop', async (req, res)=>{
    var bus_kmb = new Bus_kmb()
    res.send(inject_css(await bus_kmb.html_stop_list(req.query.stop, true)))
})

app.get('/stop/reload', async (req, res)=>{
    var bus_kmb = new Bus_kmb()
    await bus_kmb.update_stop_list('reload')
    res.json({
        'update_timestamp': bus_kmb.update_timestamp,
        'stop_list': bus_kmb.stop_list
    })
})

app.get('/stop/update', async (req, res)=>{
    var bus_kmb = new Bus_kmb()
    await bus_kmb.update_stop_list('update')
    if(req.query.silent)
        res.send('updated')
    else
        res.json({
            'update_timestamp': bus_kmb.update_timestamp,
            'stop_list': bus_kmb.stop_list
        })
})

app.get('/stop/choose/getall', async (req, res)=>{
    var bus_kmb = new Bus_kmb()
    res.send(inject_css(await bus_kmb.get_all_route_at_stop(req.query.stop, true)))
})

app.get('/route', async (req, res)=>{
    const kmb_bus_route_html = 
`<form action="/route/choose">
路線:
<input type="text" name="route"><br><br>
方向:<br>
<input type="radio" id="dir_in" name="direction" value="inbound" checked>
<label for="dir_in">Inbound</label><br>
<input type="radio" id="dir_out" name="direction" value="outbound">
<label for="dir_out">Outbound</label><br>
<br><br><input type="submit"></form>`

    res.send(inject_css(kmb_bus_route_html))
})
app.get('/route/choose', async (req, res)=>{
    var bus_kmb = new Bus_kmb()
    const results = await bus_kmb.get_route_list(req.query.route, req.query.direction, true)
    res.send(inject_css(results))
})
app.get('/route/choose/get', async (req, res)=>{
    var bus_kmb = new Bus_kmb()
    res.send(inject_css(await bus_kmb.get_route_at_stop(req.query.route, req.query.stop, true)))
})

app.use(express.json())
app.use(express.urlencoded())
app.use(express.urlencoded({extended: true}))

app.listen(process.env.PORT || 8080, () => console.log('Server ready'))